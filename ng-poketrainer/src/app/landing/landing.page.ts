import { Component } from '@angular/core';
import { LandingPageComponent } from '../landing-page/landing-page.component';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.page.html'
})

export class LandingPage {}