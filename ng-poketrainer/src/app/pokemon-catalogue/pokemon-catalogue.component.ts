import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { Pokemon } from '../models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';

@Component({
    selector: 'app-pokemon-catalogue',
    templateUrl: './pokemon-catalogue.component.html',
    styleUrls: ['./pokemon-catalogue.component.css']
}) 
export class PokemonCatalogueComponent implements OnInit{

    private existsNext: boolean = true;
    private existsPrevious: boolean = false;

    constructor(private router: Router, private readonly pokemonService: PokemonService) {}


    ngOnInit(): void {
        if (localStorage.getItem("username") === null) {
            //Route to landing page if no username is registered
            this.router.navigate(['/login']);
        }
        this.pokemonService.fetchPokemon('https://pokeapi.co/api/v2/pokemon?limit=151');
    }

    get pokemons(): Pokemon[] {
        return this.pokemonService.pokemons();
    }

    public getNextPokemons(): void {
        this.pokemonService.fetchPokemon(this.pokemonService.next());
        this.existsNext = this.pokemonService.existsNext();
    }

    public getPreviousPokemons(): void {
        this.pokemonService.fetchPokemon(this.pokemonService.previous());
        this.existsPrevious = this.pokemonService.existsPrevious();
    }

    public pokemonChosen(pokemon: Pokemon): void {
        pokemon.caught = true;
        localStorage.setItem(pokemon.name, JSON.stringify(pokemon));      
    }
    
    public goToTrainerPage(): void {
        this.router.navigate(['/trainer']);
    }
}