export interface Contact {
    id: number;
    name: string;
    deleted: boolean;
    address: ContactAddress;
}

export interface ContactAddress {
    street: string;
    city: string;
    country: string;
}