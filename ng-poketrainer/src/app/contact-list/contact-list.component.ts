import { Component, OnInit } from '@angular/core'
import { Contact } from '../models/contact.model'
import { ContactService } from '../services/contacts.service'

@Component({
    selector: 'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit{
    constructor(private readonly contactService: ContactService) {
    }

    ngOnInit(): void {
        this.contactService.fetchContacts();
    }

    get contacts(): Contact[] {
        return this.contactService.contacts();
    }

}