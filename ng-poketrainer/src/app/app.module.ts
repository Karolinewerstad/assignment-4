import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { FormsModule } from '@angular/forms';
import { LandingPage } from './landing/landing.page';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { CataloguePage } from './catalogue/catalogue.page';
import { TrainerPage } from './trainer/trainer.page';
import { TrainerPageComponent } from './trainer-page/trainer-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    LandingPageComponent,
    CataloguePage,
    PokemonCatalogueComponent,
    TrainerPage,
    TrainerPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
