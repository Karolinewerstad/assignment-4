import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Contact } from '../models/contact.model'

@Injectable( {
    providedIn: 'root'
})
export class ContactService{
    private _contacts: Contact[] = [];
    private _error: string = '';

    constructor(private readonly http: HttpClient) {
    }

    public fetchContacts(): void {
        this.http.get<Contact[]>('https://pokeapi.co/')
        .subscribe((contacts: Contact[]) => {
            this._contacts = contacts;
        }, (error: HttpErrorResponse) => {
            this._error = error.message;
        })
    }

    public contacts(): Contact[] {
        return this._contacts;
    }

    public error(): string {
        return this._error;
    }
}