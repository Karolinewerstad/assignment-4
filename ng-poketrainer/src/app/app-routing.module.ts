import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CataloguePage } from './catalogue/catalogue.page';
import { LandingPage } from './landing/landing.page';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { TrainerPage } from './trainer/trainer.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LandingPage
  },
  {
    path: 'catalogue',
    component: CataloguePage
  },
  {
    path: 'trainer',
    component: TrainerPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
