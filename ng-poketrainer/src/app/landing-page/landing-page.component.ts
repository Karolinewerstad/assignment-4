import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.css']
}) 
export class LandingPageComponent implements OnInit{

    public user: string = "inputUsername";

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        if (localStorage.getItem("username") !== null) {
            //Route to Pokémon Catalogue if a username is registered
            this.router.navigate(['/catalogue']);
        }
    }

    public onSubmit(form: NgForm) {
        this.user = form.value.user;
        localStorage.setItem("username", this.user);
        this.router.navigate(['/catalogue']);    
    }

}