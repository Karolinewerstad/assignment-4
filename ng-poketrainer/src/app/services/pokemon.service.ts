import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { ApiCall, Pokemon } from '../models/pokemon.model';
import { tap } from 'rxjs/operators'

@Injectable( {
    providedIn: 'root'
})
export class PokemonService{
    private _pokemons: Pokemon[] = [];
    private _error: string = '';
    private _fromApi: ApiCall | null = null;

    constructor(private readonly http: HttpClient) {
    }

    public fetchPokemon(url: string): void {       
        this.http.get<ApiCall>(url)
        .pipe(
            tap((data) => {
               sessionStorage.setItem('poke-cache', JSON.stringify(data));
            }
        ))
        .subscribe((data: ApiCall) => {
            this._fromApi = data
            this._pokemons = this._fromApi.results
            for (let pokemon of this._pokemons) {
                //Adding IDs for all pokemon by making a substring from the url given for each pokemon in the api.
                pokemon.id = parseInt(pokemon.url.substring(34, pokemon.url.length-1))
                //Capitalizing the names
                pokemon.name = pokemon.name.replace(pokemon.name[0], pokemon.name[0].toUpperCase());
                //Checking if pokémon has been caught already.
                if (localStorage.getItem(pokemon.name) !== null) {
                    pokemon.caught = true;
                } else {
                    pokemon.caught = false;
                }
            }
        })
    }

    public pokemons(): Pokemon[] {
        return this._pokemons;
    }

    public next(): string {
        if (this._fromApi !== null) {
            return this._fromApi.next;
        } else {
            return "";
        }
    }

    public previous(): string {
        if (this._fromApi !== null) {
            return this._fromApi.previous;
        } else {
            return "";
        }
    }

    public existsNext(): boolean {
        if (this._fromApi !== null) {
            return this._fromApi.next === null;
        }
        return false;
    }

    public existsPrevious(): boolean {
        if (this._fromApi !== null) {
            return this._fromApi.previous === null;
        }
        return false;
    }

    public error(): string {
        return this._error;
    }
}