import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { Pokemon } from '../models/pokemon.model';


@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer-page.component.html',
    styleUrls: ['./trainer-page.component.css']
}) 
export class TrainerPageComponent implements OnInit{

    private _pokemons: Pokemon[] = [];

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        if (localStorage.getItem("username") === null) {
            //Route to landing page if no username is registered
            this.router.navigate(['/login']);
        }
    }

    get caughtPokemons(): Pokemon[] {
        for (let entry in localStorage) {
            if (entry !== "username") {
                let item = localStorage.getItem(entry)
                if (item !== null) {
                    this._pokemons.push(JSON.parse(item))
                }
            }
        }
        return this._pokemons
    }
    
    //Clear local storage and redirect to login page
    public logOut(): void {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

}